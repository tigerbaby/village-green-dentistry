<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package multipurpose blog
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="header">
  <div class="container-fluid">
    <div style="display: none;" class="col-md-12 top_headbar ">
      <div class="top-bar">      
        <div class="socialbox">
          <?php if(esc_url( get_theme_mod( 'multipurpose_blog_cont_facebook','' ) ) != '') { ?>
            <a href="<?php echo esc_url( get_theme_mod( 'multipurpose_blog_cont_facebook','https://www.facebook.com/' ) ); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a>
          <?php } ?>
          <?php if(esc_url( get_theme_mod( 'multipurpose_blog_cont_twitter','' ) ) != '') { ?>
            <a href="<?php echo esc_url( get_theme_mod( 'multipurpose_blog_cont_twitter','https://twitter.com/' ) ); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a>
          <?php } ?>
          <?php if(esc_url( get_theme_mod( 'multipurpose_blog_google_plus','' ) ) != '') { ?>
            <a href="<?php echo esc_url( get_theme_mod( 'multipurpose_blog_google_plus','https://plus.google.com/' ) ); ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
          <?php } ?>
          <?php if(esc_url( get_theme_mod( 'multipurpose_blog_pinterest','' ) ) != '') { ?>
            <a href="<?php echo esc_url( get_theme_mod( 'multipurpose_blog_pinterest','https://www.pinterest.com/' ) ); ?>"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
          <?php } ?>
          <?php if(esc_url( get_theme_mod( 'multipurpose_blog_tumblr','' ) ) != '') { ?>
            <a href="<?php echo esc_url( get_theme_mod( 'multipurpose_blog_tumblr','https://www.tumblr.com/' ) ); ?>"><i class="fa fa-tumblr" aria-hidden="true"></i></a>
          <?php } ?>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3 toggle-off">
        <button id="book-appointment" class="book-appointment"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Book An Appointment</button>
      </div>
      <div class="col-sm-6 logo_bar">
        <div class="logo wow bounceInDown">
          <?php if( has_custom_logo() ){ multipurpose_blog_the_custom_logo();
             }else{ ?>
            <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php esc_attr(bloginfo( 'name' )); ?></a></h1>
            <?php $description = get_bloginfo( 'description', 'display' );
            if ( $description || is_customize_preview() ) : ?> 
              <p class="site-description"><?php echo esc_html($description); ?></p>       
          <?php endif; }?>       
        </div>
        <div class="clear"></div>
      </div>  
      <div class="col-sm-3 button-wrap toggle-off">
        <a class="tel" href="tel:1847-634-3848"><i class="fa fa-phone" aria-hidden="true"></i> 847.634.3848</a>
      </div>
    </div>
  </div>
  <div class="col-md-12 menus"> 
     <div class="toggle"><a class="toggleMenu" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a></div>
      <?php 
        $locations = get_nav_menu_locations();
        if ( isset( $locations[ 'mega_menu' ] ) ) {
      ?>
      <div class="navbar-header-toggle">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse"><?php esc_html_e('Menu','multipurpose-blog'); ?></button>
      </div>
    <?php 
      }
    ?>
    <div class="menubox header">
      <div class="nav">
       <?php wp_nav_menu( array('theme_location'  => 'primary') ); ?>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  <div class="clearfix"></div>
  <?php if( is_front_page() ) : ?>
  <div class="container-fluid splash">
    <img src="/wp-content/themes/multipurpose-blog/images/front-splash.png" />
  </div>
  <?php endif; ?> 
  <div class="container-fluid cta-buttons">
    <div class="row">
      <div class="col-sm-3"><button class="book-appointment"><i class="fa fa-calendar" aria-hidden="true"></i> Schedule Appointment</button></div>
      <div class="col-sm-3"><button onclick="window.location.href='/meet-dr-dmitry-sokol'"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Meet Dr. Sokol</button></div>
      <div class="col-sm-3"><button onclick="window.location.href='/contact'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Contact Us</button></div>
      <div class="col-sm-3"><button onclick="window.location.href='/testimonials'"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Testimonials</button></div>
    </div>
  </div>
</div><?php /** header **/ ?>