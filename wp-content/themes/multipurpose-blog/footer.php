<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package multipurpose blog
 */
?>
    <div class="footer-wp">
        <div style="display: none;" class="container">
            <div class="col-md-3 col-sm-3">
                <?php dynamic_sidebar('footer-1');?>
            </div>
            <div class="col-md-3 col-sm-3">
                <?php dynamic_sidebar('footer-2');?>
            </div>
            <div class="col-md-3 col-sm-3">
                <?php dynamic_sidebar('footer-3');?>
            </div>
            <div class="col-md-3 col-sm-3">
                <?php dynamic_sidebar('footer-4');?>
            </div>
        </div>
        <div id="vgd-dynamic-footer" class="container-fluid">
          <?php if ( is_front_page() || is_home() ) { ?>
          <div class="footer-block">
            <div class="footer-meet-dr"><img alt="" src="/wp-content/themes/multipurpose-blog/images/bg-footer-meet-dr-sokol.png">
              <div>
                <h1>MEET THE DOCTOR</h1>
              <button data-page="meet-dr-dmitry-sokol">Meet Dr Dmitry Sokol <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
              </div>
            </div>
        </div>
         <?php }?>
      </div>
    </div>      
	<div class="container-fluid footer">
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1 footer-info">
        <strong>Village Green Dentistry  &middot;  Address:</strong> 300 Village Green Drive, Suite 150 Lincolnshire, IL 60069  <strong>Phone:</strong> <a class="tel" href="tel:1-847-634-3848">847.634.3848</a>
      </div>
    </div>
<!--
        <div class="col-sm-4 col-sm-offset-1 visible-sm visible-md visible-lg">
            <h3>CONTACT US</h3>
            <p>
              <i class="fa fa-map-marker" aria-hidden="true"></i>
              300 Village Green Drive, Suite 150,<br>
              Lincolnshire, IL 60069
            </p>
            <p>
              <i class="fa fa-phone" aria-hidden="true"></i>
              <a class="tel" href="tel:1-847-634-3848">847.634.3848</a>
            </p>
            <p>
              <i class="fa fa-envelope" aria-hidden="true"></i>
              <a href="mailto:info@villagegreendentistry.com">info@villagegreendentistry.com</a>
            </p>
        </div>
        <div class="col-sm-4 footer-contact visible-sm visible-md visible-lg">
          <h3>CALL OUR OFFICE</h3>
          <a class="tel" href="tel:1-847-634-3848">847.634.3848</a>
          <button class="book-appointment">BOOK AN APPOINTMENT</button>
        </div>
        <div class="col-sm-2 footer-follow visible-sm visible-md visible-lg">
          <h3>FOLLOW US</h3>
          <h2><a title="Facebook" target="_blank" href="https://www.facebook.com/VillageGreenDentistryLincolnshire/">on Facebook</a></h2>
          <a title="Facebook" target="_blank" href="https://www.facebook.com/VillageGreenDentistryLincolnshire/">
            <i class="fa fa-facebook-official" aria-hidden="true"></i>
          </a>
        </div>
      
      mobile
        <div class="col-xs-8 col-xs-offset-2 visible-xs footer-contact">
          <h3>CALL OUR OFFICE</h3>
          <a class="tel" href="tel:1-847-634-3848">847.634.3848</a>
          <button>BOOK AN APPOINTMENT</button>
        </div>
        <div class="col-xs-8 col-xs-offset-2 visible-xs footer-follow">
          <h3>FOLLOW US</h3>
          <a title="Facebook" target="_blank" href="https://www.facebook.com/VillageGreenDentistryLincolnshire/">
            <i class="fa fa-facebook-official" aria-hidden="true"></i>
          </a>
        </div>
        <div class="col-xs-8 col-xs-offset-2 visible-xs">
            <h3>CONTACT US</h3>
            <p>
              <i class="fa fa-map-marker" aria-hidden="true"></i>
              300 Village Green Drive, Suite 150,<br>
              Lincolnshire, IL 60069
            </p>
            <p>
              <i class="fa fa-phone" aria-hidden="true"></i>
              <a class="tel" href="tel:1-847-634-3848">847.634.3848</a>
            </p>
            <p>
              <i class="fa fa-envelope" aria-hidden="true"></i>
              <a href="mailto:info@villagegreendentistry.com">info@villagegreendentistry.com</a>
            </p>
        </div>        
-->
    </div><!-- inner -->
        
    <?php wp_footer(); ?>

    </body>
</html>