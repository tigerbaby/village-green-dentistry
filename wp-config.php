<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vgd');

/** MySQL database username */
define('DB_USER', 'vgd');

/** MySQL database password */
define('DB_PASSWORD', 'vgd');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v!Kbt&]E|5aA]?c7$Ryg3E#6E~MoIK)}Kjvi0m`><C:UMy;tb}1$}e!(!k(i2EbO');
define('SECURE_AUTH_KEY',  '8^Ii00UO%cj,rndMKg|)kqn2EpV)B(JRP,OMvQC{<)<!sTbHs=%k45z>1Fdh>6v%');
define('LOGGED_IN_KEY',    '9?F8m{vx}57rxlCj+UN3MhL^QR%#-}Q+S!pabl1c5z6D}S:qPrWMZW_3LugEaFng');
define('NONCE_KEY',        'r9N>q%mlc~qmg~1v>z|`htn&v&,R-ud7bGYx.q.drf7g)1SMs&TJUlg9pF- ~,)$');
define('AUTH_SALT',        'X$*NJ6E`0Yh!|=!s>q%$<<oVC2s|6|!{u.o<.e|Yt21%t@=-f8eq*+f0fJ[~QHYm');
define('SECURE_AUTH_SALT', 'aM&s}ew:}6%2jKb.d:x<*#c$, w>4$?,%|8.xGk4-cY4@YOnRT-UZmp:`IdLnA/M');
define('LOGGED_IN_SALT',   'YVFjm{kY;3#Xu3*USLy{{yM^^kVa>&azqQMErH/)0^FMF.Xry<_wkO`5m(/:!9O3');
define('NONCE_SALT',       'SvRQh ,tYKn&U.2ldc4j:gXBq~pAw#pr_yf1 gwXZ8QOo Ox-u~8j8W0Lu]{C^Q>');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
