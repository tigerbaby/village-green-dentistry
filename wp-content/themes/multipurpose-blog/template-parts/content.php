<?php
/**
 * The template part for displaying slider
 *
 * @package multipurpose blog 
 * @subpackage multipurpose_blog
 * @since 1.0
 */
?>
<li id="post-<?php the_ID(); ?>" <?php post_class('inner-service'); ?>>
  <h3><a class="modal-link" href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title();?></a></h3>
  <div class="date-box"><a class="modal-link" href="<?php echo esc_url( get_permalink() ); ?>"><?php the_time( get_option( 'date_format' ) ); ?></a></div>
  <div class="left">
    <?php 
          if(has_post_thumbnail()) { 
            the_post_thumbnail(); 
          }
        ?>
  </div>
  <div class="right"><?php the_excerpt();?><a class="modal-link" href="<?php echo esc_url( get_permalink() ); ?>"><i class="fa fa-angle-down" aria-hidden="true"></i></a></div>
  <div class="clearfix"></div>
</li>