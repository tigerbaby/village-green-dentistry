<?php
/**
 * multipurpose blog functions and definitions
 *
 * @package multipurpose blog
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */

if ( ! function_exists( 'multipurpose_blog_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */

/* Theme Setup */
function multipurpose_blog_setup() {

	$GLOBALS['content_width'] = apply_filters( 'multipurpose_blog_content_width', 640 );
	
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'custom-logo', array(
		'height'      => 240,
		'width'       => 240,
		'flex-height' => true,
	) );
	add_image_size('multipurpose-blog-homepage-thumb',240,145,true);
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'multipurpose-blog' ),
	) );
	add_theme_support( 'custom-background', array(
		'default-color' => 'f1f1f1'
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', multipurpose_blog_font_url() ) );

}
endif; // multipurpose_blog_setup
add_action( 'after_setup_theme', 'multipurpose_blog_setup' );

/* Theme Widgets Setup */
function multipurpose_blog_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'multipurpose-blog' ),
		'description'   => __( 'Appears on posts and pages', 'multipurpose-blog' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => __( 'Posts and Pages Sidebar', 'multipurpose-blog' ),
		'description'   => __( 'Appears on posts and pages', 'multipurpose-blog' ),
		'id'            => 'sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Third Column Sidebar', 'multipurpose-blog' ),
		'description'   => __( 'Appears on posts and pages', 'multipurpose-blog' ),
		'id'            => 'sidebar-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'multipurpose-blog' ),
		'description'   => __( 'Appears on posts and pages', 'multipurpose-blog' ),
		'id'            => 'footer-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'multipurpose-blog' ),
		'description'   => __( 'Appears on posts and pages', 'multipurpose-blog' ),
		'id'            => 'footer-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 3', 'multipurpose-blog' ),
		'description'   => __( 'Appears on posts and pages', 'multipurpose-blog' ),
		'id'            => 'footer-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 4', 'multipurpose-blog' ),
		'description'   => __( 'Appears on posts and pages', 'multipurpose-blog' ),
		'id'            => 'footer-4',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'multipurpose_blog_widgets_init' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function multipurpose_blog_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", esc_url( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'multipurpose_blog_pingback_header' );

/* Theme Font URL */
function multipurpose_blog_font_url(){
	$font_url = '';
	
	/* Translators: If there are any character that are
	* not supported by PT Sans, translate this to off, do not
	* translate into your own language.
	*/
//	$ptsans = _x('on', 'PT Sans font:on or off','multipurpose-blog');
	
	/* Translators: If there are any character that are
	* not supported by Roboto, translate this to off, do not
	* translate into your own language.
	*/
	$roboto = _x('on', 'Roboto font:on or off','multipurpose-blog');
	
	/* Translators: If there are any character that are
	* not supported by Roboto Condensed, translate this to off, do not
	* translate into your own language.
	*/
	$roboto_cond = _x('on', 'Roboto Condensed font:on or off','multipurpose-blog');

	/* Translators: If there are any character that are
	* not supported by Roboto Condensed, translate this to off, do not
	* translate into your own language.
	*/
//	$dancing_cursive = _x('on', 'dancing cursive Condensed font:on or off','multipurpose-blog');

	/* Translators: If there are any character that are
	* not supported by Roboto Condensed, translate this to off, do not
	* translate into your own language.
	*/
//	$alex_brush = _x('on', 'alex brush Condensed font:on or off','multipurpose-blog');
	
	/* Translators: If there are any character that are
	* not supported by Roboto Condensed, translate this to off, do not
	* translate into your own language.
	*/
	$Overpass = _x('on', 'Overpass Condensed font:on or off','multipurpose-blog');
	
	
	if('off' !==  $roboto || 'off' !== $roboto_cond){
		$font_family = array();
		
//		if('off' !== $ptsans){
//			$font_family[] = 'PT Sans:300,400,600,700,800,900';
//		}
		
		if('off' !== $roboto){
			$font_family[] = 'Roboto:100,400,700';
            $font_family[] = 'Rasa:400,700';
		}
		
		if('off' !== $roboto_cond){
			$font_family[] = 'Roboto Condensed:400,700';
		}

//		if('off' !== $dancing_cursive){
//				$font_family[] = 'Dancing Script';
//		}

//		if('off' !== $alex_brush){
//				$font_family[] = 'Alex Brush';
//		}

//		if('off' !== $Overpass){
//                $font_family[] = 'Overpass';
//        }

		
		$query_args = array(
			'family'	=> urlencode(implode('|',$font_family)),
		);
		
		$font_url = add_query_arg($query_args,'//fonts.googleapis.com/css');
	}
	return $font_url;
}
	
/* Theme enqueue scripts */
function multipurpose_blog_scripts() {
	wp_enqueue_style( 'multipurpose-blog-font', multipurpose_blog_font_url(), array() );
	wp_enqueue_style( 'multipurpose-blog-style', get_stylesheet_uri() );
	wp_style_add_data( 'multipurpose-blog-style', 'rtl', 'replace' );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style( 'multipurpose-blog-font-awesome', get_template_directory_uri().'/css/font-awesome.css' );
	wp_enqueue_style( 'before-after', get_template_directory_uri().'/js/before-after.js-master/before-after.min.css' );
	wp_enqueue_style( 'demand-force','//www.demandforce.com/widget/css/widget.css' );
    wp_enqueue_style( 'multipurpose-blog-customcss', get_template_directory_uri() . '/css/custom.css' );
    if ( is_home() || is_front_page() ) { 
		wp_enqueue_style( 'nivo-style', get_template_directory_uri().'/css/nivo-slider.css' );
		wp_enqueue_script( 'nivo-slider', get_template_directory_uri() . '/js/jquery.nivo.slider.js', array('jquery') );
	}
	wp_enqueue_script( 'tether', get_template_directory_uri() . '/js/tether.js', array('jquery') ,'',true);
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery') ,'',true);
  wp_enqueue_script( 'before-after', get_template_directory_uri() . '/js/before-after.js-master/before-after.min.js', array('jquery') ,'',true);
	wp_enqueue_script( 'multipurpose-blog-customscripts', get_template_directory_uri() . '/js/custom.js', array('jquery') );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'multipurpose_blog_scripts' );

function multipurpose_blog_ie_stylesheet(){
	
	/** Load our IE-only stylesheet for all versions of IE.
	*   <!--[if lt IE 9]> ... <![endif]-->
	*
	*  Note: It is also possible to just check and see if the $is_IE global in WordPress is set to true before
	*  calling the wp_enqueue_style() function. If you are trying to load a stylesheet for all browsers
	*  EXCEPT for IE, then you would HAVE to check the $is_IE global since WordPress doesn't have a way to
	*  properly handle non-IE conditional comments.
	*/
	wp_enqueue_style('multipurpose-blog-ie', get_template_directory_uri().'/css/ie.css', array('multipurpose-blog-ie-style'));
	wp_style_add_data( 'multipurpose-blog-ie', 'conditional', 'IE' );
}
add_action('wp_enqueue_scripts','multipurpose_blog_ie_stylesheet');

/*radio button sanitization*/
 function multipurpose_blog_sanitize_choices( $input, $setting ) {
    global $wp_customize; 
    $control = $wp_customize->get_control( $setting->id ); 
    if ( array_key_exists( $input, $control->choices ) ) {
        return $input;
    } else {
        return $setting->default;
    }
}
/* Implement the Custom Header feature. */
require get_template_directory() . '/inc/custom-header.php';

/* Custom template tags for this theme. */
require get_template_directory() . '/inc/template-tags.php';

/* Load Jetpack compatibility file. */
require get_template_directory() . '/inc/customizer.php';

?>