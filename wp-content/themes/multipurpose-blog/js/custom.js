(function( $ ) {
	// NAVIGATION CALLBACK
	var ww = jQuery(window).width();
	jQuery(document).ready(function() { 
		jQuery(".nav li a").each(function() {
			if (jQuery(this).next().length > 0) {
				jQuery(this).addClass("parent");
			};
		})
		jQuery(".toggleMenu").click(function(e) { 
			e.preventDefault();
			jQuery(this).toggleClass("active");
			jQuery(".nav").slideToggle('fast');
		});
		adjustMenu();
	})

	// navigation orientation resize callbak
	jQuery(window).bind('resize orientationchange', function() {
		ww = jQuery(window).width();
		adjustMenu();
	});

	var adjustMenu = function() {
		if (ww < 720) {
			jQuery(".toggleMenu").css("display", "block");
			if (!jQuery(".toggleMenu").hasClass("active")) {
				jQuery(".nav").hide();
			} else {
				jQuery(".nav").show();
			}
			jQuery(".nav li").unbind('mouseenter mouseleave');
		} else {
			jQuery(".toggleMenu").css("display", "none");
			jQuery(".nav").show();
			jQuery(".nav li").removeClass("hover");
			jQuery(".nav li a").unbind('click');
			jQuery(".nav li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function() {
				jQuery(this).toggleClass('hover');
			});
		}
	}

	jQuery(document).ready(function() {
	    if( jQuery( '#slider' ).length > 0 ){
	        jQuery('.nivoSlider').nivoSlider({
                effect:'fade',
                animSpeed: 500,
                pauseTime: 3000,
                startSlide: 0,
				directionNav: true,
				controlNav: true,
				pauseOnHover:false,
	    	});
	    }
      
        var menuSections = $('#menu-menu-1 > li > a');
        menuSections.each(function(){
          var item = jQuery(this),
              list = jQuery('ul.sub-menu',item.parent());
          if(list.length === 0)return;
          item.append('<i class="fa fa-angle-right toggle" aria-hidden="true"></i>');
          jQuery('i',item).click(function(e){
            e.preventDefault();
            var subMenu = jQuery('ul.sub-menu',e.currentTarget.parentElement.parentElement);
            if(subMenu.css('display') === 'block'){
              subMenu.css('display','none');
              jQuery(e.currentTarget).removeClass('fa-angle-down').addClass('fa-angle-right');
            }
            else {
              subMenu.css('display','block');
              jQuery(e.currentTarget).removeClass('fa-angle-right').addClass('fa-angle-down'); 
            }
          });
        });
        jQuery( window ).resize(function() {
          var subMenus = jQuery( '#menu-menu-1 ul.sub-menu' ).css('display', 'none');
          subMenus.css('display', 'none');
          jQuery('#menu-menu-1 i.fa-angle-down').removeClass('fa-angle-down').addClass('fa-angle-right');
          setTimeout(function(){subMenus.css('display','');},100);
        });
      
      jQuery('.footer-block').each(function(){
        jQuery('#vgd-dynamic-footer').append(jQuery(this));
      });
      
      var teamProfiles = jQuery('.team-profile');
      if(teamProfiles.length > 0){
        teamProfiles.each(function(i){
          var profile = jQuery(this),
              img = jQuery('img',this),
              imgSrc = img.attr('src'),
              redSrc = imgSrc.replace('blue','red');
          jQuery(this).append('<img style="width: 0; height: 0;" id="profile-red-' + i + '" title="Anna" src="/wp-content/themes/multipurpose-blog/images/team/anna-red.png"/>');
          jQuery('#profile-red-' + i).load(function(){
            profile.hover(
              function() {
                img.attr('src',redSrc);
              }, function() {
                img.attr('src',imgSrc);
              }
            );
          });
        });
      }
      
      jQuery('button').each(function(){
        var button = jQuery(this),
            page = button.data('page');
        if(typeof page === 'undefined')return;
        button.click(function(){
          window.location.href = '/' + page;
        });
      });
      
//      jQuery('.footer-testimonial button').click(function(){
//        window.open('https://plus.google.com/+VillageGreenDentistryDmitrySokolDDSLincolnshire');
//      });
      
      jQuery('.book-appointment').click(function(){
        window.open('//local.demandforce.com/b/villagegreendentistry/schedule?widget=1','mywindow', 'width=790px, top=0, left=0, toolbar=no, menubar=no, scrollbars=1, resizable=1, location=no, status=0');
      });
      
      jQuery('#all-services-list li a').each(function(){
        var item = jQuery(this),
            path = window.location.pathname,
            itemLink = item.attr('href'),
            isLinkInPath = path.indexOf(itemLink) > -1;
        if(isLinkInPath){
          item.parent().append(item.html());
          item.css('display','none');
        }
      });
      
      var slides = jQuery('.slideshow-images > img'),
          slideImages,
          isImageCompare = false;
      if(slides.length === 0){
        slides = jQuery('.slideshow-images > div');
        if(slides.length > 0){
          slideImages = jQuery('> div > img',slides);
          isImageCompare = true;
        }
      }
      else slideImages = slides;
      
      if(slides.length > 0){
        function setSlideshowHeight(){return;
          var imgHeight = slideImages.eq(0).height();
          if(imgHeight === 0)return;
          jQuery('.slideshow-images').css('height', imgHeight + 'px');
          jQuery('.slideshow-arrow.top-arrow').css('height', imgHeight + 'px'); 
        }
        function goToSlide(whichWay){
          var activeImg = jQuery('.slideshow-images .active').eq(0),
              index = parseInt(activeImg.data('index')),
              next = index + whichWay;
          next = next == 0 ? slides.length : next > slides.length ? 1 : next;
          slides.each(function(){
            var tmpSlide = jQuery(this),
                interval = 700,
                tmpIndex = parseInt(tmpSlide.data('index'));
                isActive = tmpIndex == index,
                isNext = tmpIndex == next;
            if(isActive){
              tmpSlide.fadeOut(interval);
              setTimeout(function(){
                  tmpSlide.removeClass('active');
              },interval);
            }
            if(isNext){
              tmpSlide.fadeIn(interval); 
              if(isImageCompare){
                jQuery('.active-ba-slider').removeClass('active-ba-slider');
                tmpSlide.children('div').addClass('active-ba-slider');
//                jQuery('.active-ba-slider').beforeAfter(); 
              }
              setTimeout(function(){
                if(!isImageCompare)setSlideshowHeight();               
                else jQuery('#smile-gallery-image-title').html(jQuery('.active-ba-slider div:last-child img',tmpSlide).attr('title'));
                tmpSlide.addClass('active');
              },interval);
            } 
          });
        }
        setSlideshowHeight();
        slideImages.eq(0).load(function(){
          setSlideshowHeight();
        });
        $(window).resize(function(){
          setSlideshowHeight();
        });
        $("body").keydown(function(e) {
          if(e.keyCode == 37) { // left
            goToSlide(-1);
          }
          else if(e.keyCode == 39) { // right
            goToSlide(1);
          }
        });
        jQuery('.slideshow-arrow').each(function(){
          var arrow = jQuery(this),
              whichWay = arrow.hasClass('arrow-left') ? -1 : 1;
          arrow.click(function(){
            goToSlide(whichWay);
          });
        });
      }
      
	});
})( jQuery );