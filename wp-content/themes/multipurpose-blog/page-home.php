<?php

get_header(); ?>

<div class="container our-services">
  <h1>Our Services</h1>
  <div class="row">
    <div class="col-md-4 cosmetic-surgery">
      <img alt="" src="/wp-content/themes/multipurpose-blog/images/bg-cosmetic-surgery.png"/>
      <button data-page="full-mouth-reconstruction">Full Mouth Reconstruction</button>
    </div>
    <div class="col-md-4 general-dentistry">
      <img alt="" src="/wp-content/themes/multipurpose-blog/images/bg-general-dentistry.png"/>
      <button data-page="hygiene-cleanings">Cleaning</button>
    </div>
    <div class="col-md-4 dental-implants">
      <img alt="" src="/wp-content/themes/multipurpose-blog/images/bg-dental-implants.png"/>
     <button data-page="dental-implants">Dental Implants</button>
    </div>
    </div>
  <div class="row">
  <div class="col-md-4 orthodontics">
      <img alt="" src="/wp-content/themes/multipurpose-blog/images/bg-orthodontics.png"/>
      <button data-page="orthodontics-invisalign">Orthodontics</button>
    </div>
    <div class="col-md-4 latest-technology">
      <img alt="" src="/wp-content/themes/multipurpose-blog/images/bg-latest-tech.png"/>
      <button data-page="latest-technology">Latest Technology</button>
    </div>
    <div class="col-md-4 teeth-whitening">
      <img alt="" src="/wp-content/themes/multipurpose-blog/images/bg-teeth-whitening.png"/>
      <button data-page="teeth-whitening">Teeth Whitening</button>
    </div>
  </div>
</div>
<?php while ( have_posts() ) : the_post();
    the_content(); endwhile; // end of the loop. ?>
<?php get_footer(); ?>