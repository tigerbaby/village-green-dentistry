<?php
/**
 * Template Name: Page Fluid
 */

get_header(); ?>

<div style="padding:0;" id="content-tc" class="container-fluid">
    <div class="middle-align">       
        <div class="col-md-12">
            <?php            
            while ( have_posts() ) : the_post(); ?>
                <h1><?php the_title();?></h1>
                <?php if(has_post_thumbnail()) { ?>
                    <div class="feature-box">   
                        <img src="<?php the_post_thumbnail_url('full'); ?>" width="100%">
                    </div>
                <?php } ?>
                <?php the_content();

                wp_link_pages( array(
                    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'multipurpose-blog' ) . '</span>',
                    'after'       => '</div>',
                    'link_before' => '<span>',
                    'link_after'  => '</span>',
                    'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'multipurpose-blog' ) . ' </span>%',
                    'separator'   => '<span class="screen-reader-text">, </span>',
                ) );
                
                ?>               
            <?php endwhile; // end of the loop. ?>            
        </div>        
        <div class="clear"></div>    
    </div>
</div><!-- container -->

<?php get_footer(); ?>