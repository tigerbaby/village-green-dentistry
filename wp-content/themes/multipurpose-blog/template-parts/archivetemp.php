<?php
/**
 * The template part for displaying slider
 *
 * @package multipurpose blog 
 * @subpackage multipurpose_blog
 * @since 1.0
 */
?>
<?php
    the_archive_title( '<h1 class="page-title">', '</h1>' );
    the_archive_description( '<div class="taxonomy-description">', '</div>' );
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('inner-service'); ?>>    
    <div class="box-image">
      <?php 
        if(has_post_thumbnail()) { 
          the_post_thumbnail(); 
        }
      ?>	
    </div>
    <h3 class="section-title"><a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title();?></a></h3>
	  <div class="date-box"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php the_time( get_option( 'date_format' ) ); ?></a></div>
    <div class="new-text">
      <?php the_excerpt();?>
      <button style="padding: 5px 15px;" onclick="window.location.href='<?php echo esc_url( get_permalink() ); ?>'">Continue Reading</button>
    </div>	
	<div class="cat-box">
	 <?php foreach((get_the_category()) as $category) { echo esc_html($category->cat_name) . ' '; } ?>
	</div>
    <div class="clearfix"></div>
  </div>
