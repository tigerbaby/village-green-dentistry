﻿/*----------------------------------------------------------------------------*/
/* Multipurpose blog WordPress Theme */
/*----------------------------------------------------------------------------*/

Theme Name      :   Multipurpose Blog
Theme URI       :   http://www.buywptemplates.com/free/wp-multipurpose-blog-wordpress-theme/
Version         :   1.8.1.2
Tested up to    :   WP 4.8.2
Author          :   Buywptemplates
Author URI      :   http://www.buywptemplates.com/

license         :   GNU General Public License v3.0
License URI     :   http://www.gnu.org/licenses/gpl.html

Multipurpose Blog WordPress Theme, Copyright 2017 Buywptemplates
Multipurpose Blog WordPress Theme is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see .

Multipurpose Blog WordPress Theme incorporates code from VW Restaurant Lite WordPress Theme, Copyright 2016 VW Themes
VW Restaurant Lite WordPress Theme is distributed under the terms of the GNU GPL

/*----------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*----------------------------------------------------------------------------*/

email       :   support@buywptemplates.com

/*----------------------------------------------------------------------------*/
/* Theme Resources */
/*----------------------------------------------------------------------------*/

Theme is Built using the following resource bundles.

1 - Open Sans font - https://www.google.com/fonts/specimen/Open+Sans
	PT Sans font - https://fonts.google.com/specimen/PT+Sans
	Roboto font - https://fonts.google.com/specimen/Roboto
	License: Distributed under the terms of the Apache License, version 2.0 http://www.apache.org/licenses/LICENSE-2.0.html

2 - Images used from Pixabay.
	Pixabay provides images under CC0 license (https://creativecommons.org/about/cc0)
	Banner - https://www.pexels.com/photo/white-pink-rose-on-brown-wicker-basket-near-white-wall-128945/
	Fashion - https://www.pexels.com/photo/people-girl-design-happy-35188/
	Photography - https://www.pexels.com/photo/woman-in-gray-shirt-taking-a-photo-shoot-during-day-time-225227/
	Travels - https://pixabay.com/en/car-desser-alone-sunset-africa-722255/ 

3	CSS bootstrap.css
	-- Code and documentation copyright 2011-2016 Twitter, Inc. Code released under the MIT license. Docs released under Creative Commons.
	CSS nivo-slider.css
	-- Copyright 2012, Dev7studios Free to use and abuse under the MIT license.
    -- http://www.opensource.org/licenses/mit-license.php
    Font-awesome.css
    --Font Awesome 4.3.0 by @davegandy - http://fontawesome.io - @fontawesome
 	License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)

5  Customizer Licence
	All code, unless otherwise noted, is licensed under the GNU GPL, version 2 or later.
	2016 © Justin Tadlock.

6  Slider Images Url	
	https://www.pexels.com/photo/coffee-writing-computer-blogging-34658/
	https://www.pexels.com/photo/black-and-white-blog-business-coffee-261579/

All the slider images taken from pixabay under Creative Commons Deed CC0 - https://pixabay.com/en/service/terms/

All the icons taken from genericons licensed under GPL License.
http://genericons.com/

multipurpose blog Plus version
==========================================================
multipurpose blog Plus version is compatible with GPL licensed.

For any help you can mail us at support[at]buywptemplates.com

== Changelog ==

Version 1.0
1) Intial version Release

Version 1.1
1) Design Changes

Version 1.2
1) Text Changes

Version 1.3
1) css fixes

Version 1.4
1) slider fixed
2) menu fixed
3) css fixes

Version 1.5
1) Changed the layout of theme.
2) Removed depreceated functions.
3) Added the static section with category.

Version 1.6
1) resolved errors.
2) Removed unused function.

Version 1.7
1) Footer Fixed

Version 1.8.1.1
1) Resolved some errors.

Version 1.8.1.2
1) Resolved Latest Post Issue.

